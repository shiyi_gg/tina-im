# tina-im
 
#### 介绍
Tina-Im基于Netty实现 `即时通讯`

> 自动适应 `PC` `移动端` 
> 
> `发送图片`、`发送表情`

#### 软件架构
> 使用`Netty`作为即使通信、`SpringBoot`主框架
>
> `MyBatis Plus` 持久层框架、`MySQL` 存储数据
>
> `JQuery`、`JQuery.ImgBOX查看图片`、 `LayUI.Layer.mobile 提示框`
>
> `语音推送`  ...


#### 使用说明
>  图片上传 采用的是使用 `编译` 好的包，放入置于编译文件当中，这需要注意的是 建议使用 `DEBUG` 运行此项目，如果使用`RUN` 每次运行会清空 `target`编译包，导致图片
>  无法找到,
>
> 只需要在 `application.yml` 配置文件当中修改此配置即可
>
> `file:`
>
> `  domain: http://192.168.3.26:18989/`
>
> `  path: /media/tina/Hitachi/springboot/tina-im/target/classes/static/`
>
>

#### 效果图
| ![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/194814_a772fedf_5335816.png "Screenshot from 2021-01-08 19-46-16.png")   | ![输入图片说明](https://ae02.alicdn.com/kf/Hbb80f87d7fe64fff90b8b9519865363fL.png "Screenshot from 2021-01-12 17-31-37.png")  |
|---|---|
|  ![输入图片说明](https://ae01.alicdn.com/kf/H07c5cc43eb5e4d259cd0edd433eefe35H.png "在这里输入图片标题") | ![输入图片说明](https://ae01.alicdn.com/kf/H41e4a6363c1341899d1aba1f260dadaes.png "在这里输入图片标题")  |
|  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0113/171807_bd8aad47_5335816.png "Screenshot from 2021-01-13 17-17-22.png")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0113/171813_7ce439e9_5335816.png "Screenshot from 2021-01-13 17-17-09.png")  |


