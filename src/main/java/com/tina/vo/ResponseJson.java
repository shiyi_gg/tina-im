package com.tina.vo;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpStatus;

import java.util.HashMap;

/**
 * @author tina
 */
public class ResponseJson extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    private static final Integer SUCCESS_STATUS = 200;
    private static final Integer ERROR_STATUS = 100;
    private static final String SUCCESS_MSG = "OK";

    public ResponseJson() {
        super();
    }

    public ResponseJson(int code) {
        super();
        setStatus(code);
    }


    public ResponseJson(HttpStatus status) {
        super();
        setStatus(status.value());
        setMsg(status.getReasonPhrase());
    }

    public static String SUCCESS_MSG() {
        return SUCCESS_MSG;
    }

    public ResponseJson success() {
        put("msg", SUCCESS_MSG);
        put("status", SUCCESS_STATUS);
        return this;
    }

    public ResponseJson success(Object obj) {
        put("status", SUCCESS_STATUS);
        put("data", obj);
        return this;
    }

    public ResponseJson success(String msg, Object obj) {
        put("status", SUCCESS_STATUS);
        put("msg", msg);
        put("data", obj);
        return this;
    }

    public ResponseJson success(String msg) {
        put("msg", msg);
        put("status", SUCCESS_STATUS);
        return this;
    }

    public ResponseJson error(String msg) {
        put("msg", msg);
        put("status", ERROR_STATUS);
        return this;
    }

    public ResponseJson setData(String key, Object obj) {
        @SuppressWarnings("unchecked")
        HashMap<String, Object> data = (HashMap<String, Object>) get("data");
        if (data == null) {
            data = new HashMap<String, Object>();
            put("data", data);
        }
        data.put(key, obj);
        return this;
    }

    public ResponseJson setStatus(int status) {
        put("status", status);
        return this;
    }

    public ResponseJson setMsg(String msg) {
        put("msg", msg);
        return this;
    }

    public ResponseJson setValue(String key, Object val) {
        put(key, val);
        return this;
    }

    /**
     * 返回JSON字符串
     */
    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
