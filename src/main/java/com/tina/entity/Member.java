package com.tina.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 会员表
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Member implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "member_id", type = IdType.AUTO)
    private Integer memberId;

    /**
     * 用户名，账号，慕信号
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 我的头像，如果没有默认给一张
     */
    private String avatarUrl;

    /**
     * 状态
     */
    private int status;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    @TableField(exist = false)
    private List<Member> friendList;

    /**
     * 未读取好友发送消息条数
     */
    @TableField(exist = false)
    private Integer unreadMessageFriend;

    /**
     * 显示聊天最后一条数据
     */
    @TableField(exist = false)
    private String msg;

    /**
     * 显示聊天时间
     */
    @TableField(exist = false)
    private String msgTime;
}
