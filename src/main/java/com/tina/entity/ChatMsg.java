package com.tina.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 聊天记录表
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ChatMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息ID
     */
    @TableId(value = "msg_id", type = IdType.AUTO)
    private Integer msgId;

    /**
     * 发送会员ID
     */
    private Integer sendMemberId;

    /**
     * 接收会员ID
     */
    private Integer acceptMemberId;

    /**
     * 消息内容
     */
    private String msg;

    /**
     * 消息是否签收状态 true：签收 |false：未签收
     */
    private Boolean signFlag;

    /**
     * 发送请求的时间
     */
    private String createTime;


}
