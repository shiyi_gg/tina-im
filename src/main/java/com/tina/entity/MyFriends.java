package com.tina.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 好友关连表
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MyFriends implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "my_id", type = IdType.AUTO)
    private Integer myId;

    /**
     * 用户id
     */
    private Integer memberId;

    /**
     * 用户的好友id
     */
    private Integer friendMemberId;

    /**
     * 创健时间
     */
    private String createTime;


}
