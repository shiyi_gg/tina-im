package com.tina.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author tina
 * @Time 2020/12/13 上午11:06
 * @Version 1.0
 * @Content 聊天记录VO
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class MsgVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 发送消息
     */
    private List<ChatMsg> sendMsg;
    /**
     * 接收消息
     */
    private List<ChatMsg> acceptMsg;

}
