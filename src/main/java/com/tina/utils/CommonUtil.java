package com.tina.utils;


/**
 * @Author tina
 * @Time 2020/12/30 上午10:16
 * @Version 1.0
 * @Content 工具类
 **/
public class CommonUtil {

    /**
     * 正则校验url是否正确
     *
     * @param str url
     * @return
     */
    public static boolean isURL(String str) {
        //转换为小写
        str = str.toLowerCase();
        String regex = "^((https|http|ftp|rtsp|mms)?://)"  //https、http、ftp、rtsp、mms
                + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
                + "(([0-9]{1,3}\\.){3}[0-9]{1,3}" // IP形式的URL- 例如：199.194.52.184
                + "|" // 允许IP和DOMAIN（域名）
                + "([0-9a-z_!~*'()-]+\\.)*" // 域名- www.
                + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\." // 二级域名
                + "[a-z]{2,6})" // first level domain- .com or .museum
                + "(:[0-9]{1,5})?" // 端口号最大为65535,5位数
                + "((/?)|" // a slash isn't required if there is no file name
                + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        return str.matches(regex);
    }

    /**
     * 根据文件扩展名判断文件是否图片格式
     * equalsIgnoreCase 不区分大小写
     *
     * @param extension 文件扩展名
     * @return
     */
    public static boolean isImage(String extension) {
        String[] imageExtension = new String[]{"GIF", "JPEG", "JPG", "PNG"};
        for (String e : imageExtension) {
            if (extension.equalsIgnoreCase(e)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否支持该图片
     *
     * @param url
     * @return true 支持，false不支持
     */
    public static boolean standByImg(String url) {
        String ext = getExt(url);
        return isImage(ext);
    }

    /**
     * 获取图片路径URl后缀
     *
     * @param imgUrl 图片路径
     * @return
     */
    public static String getExt(String imgUrl) {
        int i = imgUrl.lastIndexOf(".");
        //获取后缀
        String ext = imgUrl.substring(i + 1);
        return ext;
    }


}
