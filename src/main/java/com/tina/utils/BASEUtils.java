package com.tina.utils;

import java.util.Base64;
import java.util.UUID;
import java.io.UnsupportedEncodingException;

/**
 * @Author tina
 * @Time 2020/12/12 下午12:14
 * @Version 1.0
 * @Content 单向加密
 **/
public class BASEUtils {


    /**
     * 加密
     *
     * @param password
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encryption(String password) {
        try {
            return Base64.getEncoder().encodeToString(password.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解密
     *
     * @param base64encoded
     * @return
     */
    public static String decrypt(String base64encoded) {
        byte[] base64decodedBytes = Base64.getDecoder().decode(base64encoded);
        try {
            return new String(base64decodedBytes, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static void main(String args[]) {
        // 使用基本编码
        String base64encodedString = encryption("1003");
        System.out.println(base64encodedString);
        System.out.println(decrypt(base64encodedString));
    }

}
