package com.tina.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tina.entity.MyFriends;
import com.tina.mapper.MyFriendsMapper;
import com.tina.service.IMyFriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 好友关连表 服务实现类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Service
public class MyFriendsServiceImpl extends ServiceImpl<MyFriendsMapper, MyFriends> implements IMyFriendsService {

    @Autowired
    private MyFriendsMapper myFriendsMapper;

    /**
     * 通过会员ID查询好友列表
     *
     * @param memberId
     * @return
     */
    @Override
    public List<MyFriends> selectFriendListByMemberId(Integer memberId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("member_id", memberId);
        return myFriendsMapper.selectList(queryWrapper);
    }
}
