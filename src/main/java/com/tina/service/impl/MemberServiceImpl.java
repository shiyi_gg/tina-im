package com.tina.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tina.entity.Member;
import com.tina.entity.MyFriends;
import com.tina.mapper.MemberMapper;
import com.tina.service.IChatMsgService;
import com.tina.service.IMemberService;
import com.tina.service.IMyFriendsService;
import com.tina.utils.BASEUtils;
import com.tina.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements IMemberService {

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private IMyFriendsService myFriendsService;

    @Autowired
    private IChatMsgService chatMsgService;

    /**
     * 新增会员
     *
     * @param member
     * @return
     */
    @Override
    public Integer insertMember(Member member) {
        member.setAvatarUrl("http://admin.tinaroot.cn/static/img/logo.e9869cf7.png");
        member.setCreateTime(DateUtils.getTime());
        member.setPassword(BASEUtils.encryption(member.getPassword()));
        return memberMapper.insert(member);
    }

    /**
     * 查询会员
     *
     * @param member
     * @return
     */
    @Override
    public Member selectMember(Member member) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_name", member.getUserName());
        queryWrapper.eq("password", BASEUtils.encryption(member.getPassword()));
        return memberMapper.selectOne(queryWrapper);
    }

    /**
     * 通过ID查询会员信息
     *
     * @param memberId
     * @return
     */
    @Override
    public Member selectMemberById(Integer memberId) {
        return memberMapper.selectById(memberId);
    }

    /**
     * 查询好友
     *
     * @param memberId
     * @return
     */
    @Override
    public Member selectFriends(Integer memberId) {
        Member member = selectMemberById(memberId);
        return member;
    }


    /**
     * 获取好友列表
     *
     * @param memberId
     * @return
     */
    @Override
    public List<Member> getFriendList(Integer memberId) {
        List<Member> list = new ArrayList<>();
        List<MyFriends> myFriends = myFriendsService.selectFriendListByMemberId(memberId);
        if (myFriends.size() > 0) {
            for (MyFriends myFriend : myFriends) {
                list.add(selectMemberById(myFriend.getFriendMemberId()));
            }
        }
        return list;
    }

    /**
     * 校验用户是否存在
     * @param name 会员名
     * @return
     */
    @Override
    public boolean checkeMember(String name) {
        LambdaQueryWrapper<Member> wrapper=new LambdaQueryWrapper<>();
        wrapper.eq(Member::getUserName,name);
        wrapper.ne(Member::getStatus,2);
        List<Member> members = memberMapper.selectList(wrapper);
        return members.size()>0;
    }


}
