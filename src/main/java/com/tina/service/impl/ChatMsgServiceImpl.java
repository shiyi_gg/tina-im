package com.tina.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tina.entity.ChatMsg;
import com.tina.entity.MsgVo;
import com.tina.mapper.ChatMsgMapper;
import com.tina.service.IChatMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 聊天记录表 服务实现类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
@Service
public class ChatMsgServiceImpl extends ServiceImpl<ChatMsgMapper, ChatMsg> implements IChatMsgService {

    @Autowired
    private ChatMsgMapper chatMsgMapper;

    /**
     * 查询聊天记录
     *
     * @param chatMsg
     * @return
     */
    @Override
    public List<ChatMsg> selectChatMsg(ChatMsg chatMsg) {
        List<ChatMsg> sendMsg = sendMsg(chatMsg);
        List<ChatMsg> acceptMsg = acceptMsg(chatMsg);
        sendMsg.addAll(acceptMsg);
        List<ChatMsg> collect = sendMsg.stream().sorted(Comparator.comparing(ChatMsg::getCreateTime)).collect(Collectors.toList());
        return collect;
    }

    /**
     * 修改未读取消息
     *
     * @param chatMsg
     * @return
     */
    @Override
    public Integer editUnreadMessage(ChatMsg chatMsg) {
        UpdateWrapper wrapper = new UpdateWrapper();
        wrapper.eq("send_member_id", chatMsg.getAcceptMemberId());
        wrapper.eq("accept_member_id", chatMsg.getSendMemberId());
        chatMsg.setSignFlag(true);
        chatMsg.setAcceptMemberId(null);
        chatMsg.setSendMemberId(null);
        return chatMsgMapper.update(chatMsg, wrapper);
    }

    /**
     * 新增聊天记录
     *
     * @param chatMsg
     * @return
     */
    @Override
    public Integer insertChatMsg(ChatMsg chatMsg) {
        return chatMsgMapper.insert(chatMsg);
    }

    /**
     * 查询好友未读取的消息条数
     *
     * @param sendMemberId
     * @return
     */
    @Override
    public Integer unreadMessageNum(Integer sendMemberId, Integer acceptMemberId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("send_member_id", sendMemberId);
        queryWrapper.eq("accept_member_id", acceptMemberId);
        queryWrapper.eq("sign_flag", false);
        return chatMsgMapper.selectCount(queryWrapper);
    }

    /**
     * 发送消息
     *
     * @param chatMsg
     * @return
     */
    private List<ChatMsg> sendMsg(ChatMsg chatMsg) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("send_member_id", chatMsg.getSendMemberId());
        queryWrapper.eq("accept_member_id", chatMsg.getAcceptMemberId());
        return chatMsgMapper.selectList(queryWrapper);
    }

    /**
     * 接收消息
     *
     * @param chatMsg
     * @return
     */
    private List<ChatMsg> acceptMsg(ChatMsg chatMsg) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("send_member_id", chatMsg.getAcceptMemberId());
        queryWrapper.eq("accept_member_id", chatMsg.getSendMemberId());
        return chatMsgMapper.selectList(queryWrapper);
    }


}
