package com.tina.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tina.entity.Member;

import java.util.List;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface IMemberService extends IService<Member> {

    /**
     * 新增 会员
     *
     * @param member
     * @return
     */
    public Integer insertMember(Member member);

    /**
     * 查询会员
     *
     * @param member
     * @return
     */
    public Member selectMember(Member member);

    /**
     * 通过ID查询会员信息
     *
     * @param memberId
     * @return
     */
    public Member selectMemberById(Integer memberId);

    /**
     * 查询好友列表
     *
     * @param memberId
     * @return
     */
    public Member selectFriends(Integer memberId);

    /**
     * 获取好友列表
     *
     * @param memberId
     * @return
     */
    public List<Member> getFriendList(Integer memberId);

    /**
     * 校验用户是否存在
     * @param name 会员名
     * @return
     */
    public boolean checkeMember(String name);
}
