package com.tina.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tina.entity.MyFriends;

import java.util.List;

/**
 * <p>
 * 好友关连表 服务类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface IMyFriendsService extends IService<MyFriends> {

    /**
     * 通过会员ID查询好友列表
     *
     * @param memberId
     * @return
     */
    List<MyFriends> selectFriendListByMemberId(Integer memberId);

}
