package com.tina.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tina.entity.ChatMsg;
import com.tina.entity.MsgVo;
import com.tina.mapper.ChatMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 * 聊天记录表 服务类
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface IChatMsgService extends IService<ChatMsg> {

    /**
     * 查询聊天记录
     *
     * @param chatMsg
     * @return
     */
    public List<ChatMsg> selectChatMsg(ChatMsg chatMsg);

    /**
     * 修改未读取消息
     *
     * @param chatMsg
     * @return
     */
    public Integer editUnreadMessage(ChatMsg chatMsg);

    /**
     * 新增聊天记录
     *
     * @param chatMsg
     * @return
     */
    public Integer insertChatMsg(ChatMsg chatMsg);

    /**
     * 未读取消息
     *
     * @param sendMemberId
     * @return
     */
    public Integer unreadMessageNum(Integer sendMemberId,Integer acceptMemberId);
}
