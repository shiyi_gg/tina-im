package com.tina.service;

import com.alibaba.fastjson.JSONObject;
import com.tina.entity.ChatMsg;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

/**
 * @author tina
 */
public interface ChatService {

    /**
     * 第一次注册
     *
     * @param memberId
     * @param ctx
     */
    public void register(Integer memberId, ChannelHandlerContext ctx);

    /**
     * 发送消息
     *
     * @param chatMsg
     */
    public void singleSend(ChatMsg chatMsg);

    /**
     * 查询聊天记录
     *
     * @param chatMsg
     * @param ctx
     * @return
     */
    public void selectChatMsgSend(ChatMsg chatMsg, ChannelHandlerContext ctx);


}
