package com.tina.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tina.entity.MyFriends;

/**
 * <p>
 * 好友关连表 Mapper 接口
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface MyFriendsMapper extends BaseMapper<MyFriends> {

}
