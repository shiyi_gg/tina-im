package com.tina.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tina.entity.Member;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface MemberMapper extends BaseMapper<Member> {

}
