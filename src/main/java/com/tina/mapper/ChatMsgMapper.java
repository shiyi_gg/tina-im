package com.tina.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tina.entity.ChatMsg;

/**
 * <p>
 * 聊天记录表 Mapper 接口
 * </p>
 *
 * @author Tina
 * @since 2020-12-12
 */
public interface ChatMsgMapper extends BaseMapper<ChatMsg> {

}
