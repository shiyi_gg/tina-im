package com.tina.netty;

import com.tina.entity.ChatMsg;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 接收类型
 *
 * @author tina
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DataContent implements Serializable {
    /**
     * 动作类型
     */
    private String type;
    private Integer memberId;
    private ChatMsg chatMsg;
}
