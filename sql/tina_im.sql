/*
 Navicat Premium Data Transfer

 Source Server         : tinaroot.cn
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : tinaroot.cn:3306
 Source Schema         : tina_im

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 19/01/2022 15:47:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat_msg
-- ----------------------------
DROP TABLE IF EXISTS `chat_msg`;
CREATE TABLE `chat_msg` (
  `msg_id` int NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `send_member_id` int NOT NULL COMMENT '发送会员ID',
  `accept_member_id` int NOT NULL COMMENT '接收会员ID',
  `msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '消息内容',
  `sign_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '消息是否签收状态\r\n1：签收\r\n0：未签收\r\n',
  `create_time` datetime NOT NULL COMMENT '发送请求的时间',
  PRIMARY KEY (`msg_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='聊天记录表';

-- ----------------------------
-- Records of chat_msg
-- ----------------------------
BEGIN;
INSERT INTO `chat_msg` VALUES (225, 1, 2, '132', 1, '2020-12-29 10:22:32');
INSERT INTO `chat_msg` VALUES (226, 1, 2, '123', 1, '2020-12-29 10:23:09');
INSERT INTO `chat_msg` VALUES (227, 2, 1, '姑', 1, '2020-12-29 10:23:46');
INSERT INTO `chat_msg` VALUES (228, 2, 1, '返回', 1, '2020-12-29 10:23:54');
INSERT INTO `chat_msg` VALUES (229, 2, 1, '....', 1, '2020-12-29 10:24:00');
INSERT INTO `chat_msg` VALUES (230, 1, 2, '额额外<br><br>', 1, '2020-12-29 10:24:09');
INSERT INTO `chat_msg` VALUES (231, 2, 1, 'fy ', 1, '2020-12-29 10:24:18');
INSERT INTO `chat_msg` VALUES (232, 2, 1, 'gj  ', 1, '2020-12-29 10:24:28');
INSERT INTO `chat_msg` VALUES (233, 2, 1, 'vvvg ', 1, '2020-12-29 10:24:57');
INSERT INTO `chat_msg` VALUES (234, 3, 1, 'ghh ', 1, '2020-12-29 10:25:13');
INSERT INTO `chat_msg` VALUES (235, 3, 1, 'ghh ', 1, '2020-12-29 10:26:29');
INSERT INTO `chat_msg` VALUES (236, 3, 1, 'mjkkknb', 1, '2020-12-29 10:26:40');
INSERT INTO `chat_msg` VALUES (237, 1, 3, '报告称', 1, '2020-12-29 10:26:47');
INSERT INTO `chat_msg` VALUES (238, 1, 3, '312', 1, '2020-12-29 10:27:13');
INSERT INTO `chat_msg` VALUES (239, 3, 1, 'fg ', 1, '2020-12-29 10:27:17');
INSERT INTO `chat_msg` VALUES (240, 3, 1, '复活甲', 1, '2020-12-29 10:38:43');
INSERT INTO `chat_msg` VALUES (241, 3, 1, '厚', 1, '2020-12-29 10:39:48');
INSERT INTO `chat_msg` VALUES (242, 1, 3, '是啊又要为工作而奔波<br><br>', 1, '2020-12-29 10:40:08');
INSERT INTO `chat_msg` VALUES (243, 3, 1, '姑', 1, '2020-12-29 10:40:25');
INSERT INTO `chat_msg` VALUES (244, 1, 3, '阿萨德', 1, '2020-12-29 10:42:43');
INSERT INTO `chat_msg` VALUES (245, 1, 3, '的', 1, '2020-12-29 11:03:15');
INSERT INTO `chat_msg` VALUES (246, 3, 1, 'fds', 1, '2020-12-29 11:03:37');
INSERT INTO `chat_msg` VALUES (247, 3, 1, '姑', 1, '2020-12-29 11:04:54');
INSERT INTO `chat_msg` VALUES (248, 3, 1, 'aa', 1, '2020-12-29 11:08:19');
INSERT INTO `chat_msg` VALUES (249, 2, 1, 'dsa', 1, '2020-12-29 11:08:46');
INSERT INTO `chat_msg` VALUES (250, 3, 1, 'sa', 1, '2020-12-29 11:08:55');
INSERT INTO `chat_msg` VALUES (251, 3, 1, '213', 1, '2020-12-29 11:08:56');
INSERT INTO `chat_msg` VALUES (252, 1, 3, '我是Tina<br><br>', 1, '2020-12-29 11:15:53');
INSERT INTO `chat_msg` VALUES (253, 3, 1, '我是A', 1, '2020-12-29 11:16:13');
INSERT INTO `chat_msg` VALUES (254, 3, 1, '啊', 1, '2020-12-29 11:16:28');
INSERT INTO `chat_msg` VALUES (255, 3, 1, '撒', 1, '2020-12-29 11:16:34');
INSERT INTO `chat_msg` VALUES (256, 1, 2, '大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大大', 1, '2020-12-29 11:38:06');
INSERT INTO `chat_msg` VALUES (257, 3, 1, '胡迪达斯', 1, '2020-12-30 00:00:40');
INSERT INTO `chat_msg` VALUES (258, 3, 1, '糊', 1, '2020-12-30 00:00:44');
INSERT INTO `chat_msg` VALUES (259, 3, 1, '互动', 1, '2020-12-30 00:01:33');
INSERT INTO `chat_msg` VALUES (260, 1, 3, '还记得我', 1, '2020-12-30 00:01:48');
INSERT INTO `chat_msg` VALUES (261, 1, 3, 'sda', 1, '2021-01-07 17:51:49');
INSERT INTO `chat_msg` VALUES (262, 1, 2, '发育', 1, '2021-01-07 17:58:21');
INSERT INTO `chat_msg` VALUES (263, 1, 2, '姑u', 1, '2021-01-07 17:58:27');
INSERT INTO `chat_msg` VALUES (264, 1, 2, '姑', 1, '2021-01-07 17:58:30');
INSERT INTO `chat_msg` VALUES (265, 2, 1, 'dsa', 1, '2021-01-07 17:58:52');
INSERT INTO `chat_msg` VALUES (266, 2, 1, 'gduw', 1, '2021-01-07 18:02:34');
INSERT INTO `chat_msg` VALUES (267, 1, 2, 'hu', 1, '2021-01-07 18:02:44');
INSERT INTO `chat_msg` VALUES (268, 1, 2, 'hh', 1, '2021-01-07 18:02:48');
INSERT INTO `chat_msg` VALUES (269, 2, 1, 'guu', 1, '2021-01-07 18:04:02');
INSERT INTO `chat_msg` VALUES (270, 2, 1, 'bhhki ', 1, '2021-01-07 18:05:02');
INSERT INTO `chat_msg` VALUES (271, 1, 2, 'fas', 1, '2021-01-08 16:04:25');
INSERT INTO `chat_msg` VALUES (272, 2, 1, 'hello Tina<br>', 1, '2021-01-08 19:41:37');
INSERT INTO `chat_msg` VALUES (273, 3, 1, '13', 1, '2021-01-08 19:42:25');
INSERT INTO `chat_msg` VALUES (274, 3, 1, '132', 1, '2021-01-08 19:42:30');
INSERT INTO `chat_msg` VALUES (275, 2, 1, 'dwa', 1, '2021-01-08 19:42:33');
INSERT INTO `chat_msg` VALUES (276, 1, 2, 'dsa', 1, '2021-01-08 19:42:38');
INSERT INTO `chat_msg` VALUES (277, 2, 1, 'dsa', 1, '2021-01-08 19:42:49');
INSERT INTO `chat_msg` VALUES (278, 3, 1, 'dsa', 1, '2021-01-08 19:42:52');
INSERT INTO `chat_msg` VALUES (279, 1, 2, 'dsa', 1, '2021-01-08 19:42:55');
INSERT INTO `chat_msg` VALUES (280, 1, 2, 'dsa', 1, '2021-01-08 19:43:59');
INSERT INTO `chat_msg` VALUES (281, 3, 1, 'dsa', 1, '2021-01-08 19:44:04');
INSERT INTO `chat_msg` VALUES (282, 1, 3, 'd1111', 1, '2021-01-08 19:44:11');
INSERT INTO `chat_msg` VALUES (283, 3, 1, '54332', 1, '2021-01-08 19:44:17');
INSERT INTO `chat_msg` VALUES (284, 3, 1, '312', 1, '2021-01-08 19:44:23');
INSERT INTO `chat_msg` VALUES (285, 1, 3, '231', 1, '2021-01-08 19:44:27');
INSERT INTO `chat_msg` VALUES (286, 1, 3, '2', 1, '2021-01-08 19:44:31');
INSERT INTO `chat_msg` VALUES (287, 3, 1, '31', 1, '2021-01-08 19:44:37');
INSERT INTO `chat_msg` VALUES (288, 1, 2, 'aaa<br><br>', 1, '2022-01-19 11:22:03');
INSERT INTO `chat_msg` VALUES (289, 2, 1, '1123<br><br>', 0, '2022-01-19 11:22:44');
INSERT INTO `chat_msg` VALUES (290, 1, 2, '<span class=\"emoji-picker-image\" style=\"background-position: -102px -52px;\"></span>', 0, '2022-01-19 11:22:55');
INSERT INTO `chat_msg` VALUES (291, 1, 3, ',,,<br><br>', 0, '2022-01-19 12:15:51');
INSERT INTO `chat_msg` VALUES (292, 1, 3, '<div><a href=\'http://im.images.tinaroot.cn/1642565755874.jpg\'> <img src=\'http://im.images.tinaroot.cn/1642565755874.jpg\' /></a></img><div>', 0, '2022-01-19 12:15:55');
COMMIT;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `member_id` int NOT NULL AUTO_INCREMENT COMMENT '会员ID',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名，账号，慕信号',
  `password` varchar(64) NOT NULL COMMENT '密码',
  `avatar_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '我的头像，如果没有默认给一张',
  `status` int DEFAULT '0' COMMENT '状态(0正常，1禁言，2删除)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间\n',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间\n',
  PRIMARY KEY (`member_id`) USING BTREE,
  UNIQUE KEY `id` (`member_id`),
  UNIQUE KEY `username` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='会员表';

-- ----------------------------
-- Records of member
-- ----------------------------
BEGIN;
INSERT INTO `member` VALUES (1, 'Tina', 'MTAwMw==', 'https://pic.rmb.bdstatic.com/bjh/f366f15341e10c4901c436cbb473fd04.png', 0, NULL, NULL);
INSERT INTO `member` VALUES (2, 'Erin', 'MTAwMw==', 'https://s3.ax1x.com/2020/12/12/rVll1e.jpg', 0, NULL, NULL);
INSERT INTO `member` VALUES (3, 'a', 'YQ==', 'https://s3.ax1x.com/2020/12/12/rVlNAP.jpg', 0, NULL, NULL);
INSERT INTO `member` VALUES (4, 'b', 'Yg==', 'https://s3.ax1x.com/2020/12/12/rVl01g.jpg', 0, NULL, NULL);
INSERT INTO `member` VALUES (6, 'TinaRoot', 'MTAwMw==', 'http://admin.tinaroot.cn/static/img/logo.e9869cf7.png', 0, '2022-01-19 11:50:14', NULL);
COMMIT;

-- ----------------------------
-- Table structure for my_friends
-- ----------------------------
DROP TABLE IF EXISTS `my_friends`;
CREATE TABLE `my_friends` (
  `my_id` int NOT NULL AUTO_INCREMENT,
  `member_id` int NOT NULL COMMENT '用户id',
  `friend_member_id` int NOT NULL COMMENT '用户的好友id',
  `create_time` datetime DEFAULT NULL COMMENT '创健时间',
  PRIMARY KEY (`my_id`) USING BTREE,
  UNIQUE KEY `my_user_id` (`member_id`,`friend_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='好友关连表';

-- ----------------------------
-- Records of my_friends
-- ----------------------------
BEGIN;
INSERT INTO `my_friends` VALUES (1, 1, 2, '2020-12-12 15:28:16');
INSERT INTO `my_friends` VALUES (2, 1, 3, '2020-12-12 15:28:25');
INSERT INTO `my_friends` VALUES (3, 1, 4, '2020-12-12 15:38:59');
INSERT INTO `my_friends` VALUES (4, 3, 1, '2020-12-13 10:26:30');
INSERT INTO `my_friends` VALUES (5, 2, 1, '2020-12-13 14:49:19');
INSERT INTO `my_friends` VALUES (6, 4, 1, '2020-12-13 14:49:17');
INSERT INTO `my_friends` VALUES (7, 2, 3, '2020-12-13 14:49:29');
INSERT INTO `my_friends` VALUES (8, 3, 2, '2020-12-13 14:49:37');
INSERT INTO `my_friends` VALUES (9, 4, 2, '2020-12-13 14:49:45');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
